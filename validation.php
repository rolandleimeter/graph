<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $em=(float)$_POST['em'];
    $km=(float)$_POST['km'];
    $ks=(float)$_POST['ks'];
    $szs=(float)$_POST['szs'];
    $d=(float)$_POST['d'];
    $mx=(float)$_POST['mx'];
    $vc=(float)$_POST['vc'];
    $kc=(float)$_POST['kc'];
    $stab= $_POST['sk'];
    $felsz= $_POST['tf'];
    $period=$_POST['pe'];
    $szennyanyag=$_POST['szm'];
    $alt=(float)$_POST['alt'];
    $imh=(float)$_POST['imh'];


//Emisszió tömegárama
    if (empty($em)){
         $error_message = "Emisszió tömegárama mező kitöltése kötelező!";
    } elseif($em < 0) {
         $error_message = "Emisszió tömegárama nem lehet negatív!";
    }


//Modell max. távolság
    if (empty($mx)){
         $error_message = "A modell max. távolság mező kitöltése kötelező!";
    }


//Környezeti levegő hőmérséklete
    if (is_null($kc)){
         $error_message = "A környezeti levegő hőmérséklete mező kitöltése kötelező!";
    } elseif($kc < -50){
    	 $error_message = "A környezeti levegő hőmérséklete nem lehet -50 °C-nál kevesebb!";
    } elseif($kc > 50){
    	 $error_message = "A környezeti levegő hőmérséklete nem lehet 50 °C-nál nagyobb!";
    }


//Szélsebesség
    if($szs < 0) {
         $error_message = "A szélsebesség nem lehet negatív!";
    } elseif($szs > 100){
    	 $error_message = "A szélsebesség nem lehet több, mint 100 m/s!";
    }


//Véggáz kilépési sebessége
    if (empty($ks)){
         $error_message = "A véggáz kilépési sebessége mező kitöltése kötelező!";
    } elseif($ks < 0) {
         $error_message = "A véggáz kilépési sebessége nem lehet negatív!";
    }


//Véggáz hőmérséklete
    if (is_null($vc)){
         $error_message = "A véggáz hőmérséklete mező kitöltése kötelező!";
    } elseif($vc < -100){
    	 $error_message = "A véggáz hőmérséklete nem lehet -100°C-nál kevesebb!";
    }


//Kéményátmérő
    if (empty($d)){
         $error_message = "A kéményátmérő mező kitöltése kötelező!";
    } elseif($d < 0) {
         $error_message = "A kéményátmérő nem lehet negatív!";
    }

// Kéménymagasság
    if (empty($km)){
         $error_message = "A kéménymagasság mező kitöltése kötelező!";
    } elseif($km < 0) {
         $error_message = "A kéménymagasság nem lehet negatív!";
    }


//
    if(!isset($error_message)) {
		$result = print_r('<div id="curve_chart" style="width: 900px; height: 500px"></div><h2>Letöltés: <img id="chartImg" /></h2>');
		if(!empty($result)) {
			$error_message = "";
			$success_message = "Sikeres adatbevitel!";	
		} else {
			$error_message = "Sikertelen adatbevitel! Próbáld újra!";	
		}
	}
}

?>