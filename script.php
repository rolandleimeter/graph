<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var ht='';
            if(document.getElementById('pe').value==1 || document.getElementById('szm').value==5 && document.getElementById('pe').value==2){
                ht = 'Hatásterület (c): <?php echo $c_tav ?> m';
            }
            var htab='<?php echo $ht ?>';
            if(document.getElementById('pe').value==2 || document.getElementById('pe').value==3){
                htab='';
            }

            var data = '';
            <?php if ($hatasterulet > 0) { ?>
            var data = google.visualization.arrayToDataTable([
                ['','Koncentráció, max: <?php echo round($maxkonc, 3) ?>', ht,htab],

                //Koncentráció számítása
                <?php
                $konc=0;

                for ($x = 1; $x <= $_POST['mx']; $x++) {

                    $Y_kitevo=0.367*(2.5-$st);

                    $szigY=0.08*(((6*(pow($st, -0.3)))+1-(log($H/$z0)))*(pow($x, $Y_kitevo)));

                    $Z_kitevo=1.55*exp(-2.35*$st);

                    $szigZ=0.38*pow($st, 1.3)*(8.7-log($H/$z0))*pow($x, $Z_kitevo);

                    if($period==1){
                    $konc=(($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ));

                    }elseif($period==2){
                    $konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/24),0.45));

                    }else{
                    $konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/8760),0.45));

                    }

                    $konc=round($konc, 3);



                    switch (true) {

                        case ($x < $c_tav):

                            echo "[" . $x . ", " . $konc . ",".$c.",".$hatasterulet."],";
                            break;


                        case ($x > $c_tav - 1 && $x < $c_tav + 1):

                            echo "[" . $x . ", " . $konc . ", " . $vert . ",".$vert."],";
                            break;


                        case ($x > $c_tav):

                            echo "[" . $x . ", " . $konc . ", null, null],";
                            break;



                        case ($x < $hatasterulet):

                            echo "[" . $x . ", " . $konc . ",".$hatasterulet.",".$hatasterulet."],";
                            break;


                        case ($x > $hatasterulet - 1 && $x < $hatasterulet + 1):

                            echo "[" . $x . ", " . $konc . ", " . $vert . ",".$vert."],";
                            break;


                        case ($x > $hatasterulet):

                            echo "[" . $x . ", " . $konc . ", null, null],";
                            break;



                    }
                        }
?>
            ]);
            <?php }else{ ?>

            var data = google.visualization.arrayToDataTable([
                ['','Koncentráció, max: <?php echo round($maxkonc, 3) ?>', ht],

                //Koncentráció számítása
                <?php
                $konc=0;

                for ($x = 1; $x <= $_POST['mx']; $x++) {

                    $Y_kitevo=0.367*(2.5-$st);

                    $szigY=0.08*(((6*(pow($st, -0.3)))+1-(log($H/$z0)))*(pow($x, $Y_kitevo)));

                    $Z_kitevo=1.55*exp(-2.35*$st);

                    $szigZ=0.38*pow($st, 1.3)*(8.7-log($H/$z0))*pow($x, $Z_kitevo);

                    if($period==1){
                        $konc=(($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ));

                    }elseif($period==2){
                        $konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/24),0.45));

                    }else{
                        $konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/8760),0.45));

                    }

                    $konc=round($konc, 3);


                    switch (true) {

                        case ($x<$c_tav):

                            echo "[".$x.", ".$konc.",".$c."],";

                            break;


                        case ($x>$c_tav-1 && $x<$c_tav+1):

                            echo "[".$x.", ".$konc.", ".$vert."],";
                            break;



                        case ($x>$c_tav):

                            echo "[".$x.", ".$konc.", null],";
                            break;

                    }
                }
                ?>
            ]);

            <?php } ?>

var htsz='';
var hta = '';
if(document.getElementById('pe').value==1 || document.getElementById('szm').value==5 && document.getElementById('pe').value==2){
    htsz = '#ffa100';
    hta = '#001dff';
}
if(document.getElementById('szm').value==5 && document.getElementById('pe').value==1){
    htsz ='';
    hta = '';
            }

            var options = {
                curveType: 'function',
                legend: { position: 'right' },
                colors: ['#000000', htsz, hta, '#ff0000'],
                animation:{
                    duration: document.getElementById('mx').value,
                    easing: 'linear',
                    startup: true,
                },
                vAxis: {title: 'Koncentráció [ µg/m³ ]',
                        minValue:0,
                    ticks: [<?php echo (0.25*$maxtick).",".(0.5*$maxtick).",".(0.75*$maxtick).",".($maxtick)?>]},
                hAxis: {title: 'Távolság [m]',
                        minValue:0, maxValue:document.getElementById('mx').value},
                series: {
                    1: { lineDashStyle: [2, 2] }
                }
            };

            var appliedTickSize = 0.25;
            
            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));



           
google.visualization.events.addListener(chart, 'ready', function () {
        var imgUri = chart.getImageURI();
        // do something with the image URI, like:
        document.getElementById('chartImg').src = imgUri;
        document.getElementById('chartImg').outerHTML = '<a href="' + chart.getImageURI() +'" target="_blank">Kép megnyitása új lapon</a>';
    });
            chart.draw(data, options);
        }

function onSelectChange(){
document.getElementById('szm').addEventListener('click', function() {
    document.getElementById('textbox').hidden = true;
    $("#imh").prop('readonly',true); 
    //Kén-monoxid
    if (this.value == 2 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 250;
    }else if (this.value == 2 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 125;
    }else if (this.value == 2 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 50;
    }
    //Nitrogén-dioxid
    else if (this.value == 3 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 100;
    }else if (this.value == 3 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 80;
    }else if (this.value == 3 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 40;
    }
    //Szén-monoxid
    else if (this.value == 4 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 10000;
    }else if (this.value == 4 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 5000;
    }else if (this.value == 4 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 3000;
    }
    //Szálló por
    else if (this.value == 5 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 1;
    }else if (this.value == 5 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 50;
    }else if (this.value == 5 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 40;
    }
    //Egyéb
     else if (this.value == 6) {
        document.getElementById('textbox').hidden = false; 
        $("#imh").prop('readonly',false); 
    }  else {
        document.getElementById('textbox').hidden = true;
        $("#imh").prop('readonly',false); 
    }

});
};

function onPeriodChange(){
document.getElementById('pe').addEventListener('click', function() {
    document.getElementById('textbox').hidden = true;
    $("#imh").prop('readonly',true); 
    //Kén-monoxid
    if (document.getElementById('szm').value == 2 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 250;
    }else if (document.getElementById('szm').value == 2 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 125;
    }else if (document.getElementById('szm').value == 2 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 50;
    }
    //Nitrogén-dioxid
    else if (document.getElementById('szm').value == 3 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 100;
    }else if (document.getElementById('szm').value == 3 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 80;
    }else if (document.getElementById('szm').value == 3 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 40;
    }
    //Szén-monoxid
    else if (document.getElementById('szm').value == 4 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 10000;
    }else if (document.getElementById('szm').value == 4 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 5000;
    }else if (document.getElementById('szm').value == 4 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 3000;
    }
    //Szálló por
    else if (document.getElementById('szm').value == 5 && document.getElementById('pe').value==1){
        document.getElementById('imh').value = 1;
    }else if (document.getElementById('szm').value == 5 && document.getElementById('pe').value==2){
        document.getElementById('imh').value = 50;
    }else if (document.getElementById('szm').value == 5 && document.getElementById('pe').value==3){
        document.getElementById('imh').value = 40;
    }
    //Egyéb
     else if (document.getElementById('szm').value == 6) {
        document.getElementById('textbox').hidden = false;
        $("#imh").prop('readonly',false); 
    }  else {
        document.getElementById('textbox').hidden = true;
        $("#imh").prop('readonly',false); 
    }

});
};

</script>