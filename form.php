<form id="columnarForm" action="" method="post">

<?php
if(isset($_POST['submit'])){
 $imh=$_POST['imh'];
}
?>
Kéménymagasság [m]: *<input type="number" step="any" min="0"  name="km" id="km" value="<?php echo htmlspecialchars($_POST['km']); ?>"><br>

Kéményátmérő [m]: *<input type="number" step="any" min="0" name="d" id="d"  value="<?php echo htmlspecialchars($_POST['d']); ?>"><br>

Véggáz hőmérséklete [°C]: *<input type="number" step="any"  name="vc" id="vc" value="<?php echo htmlspecialchars($_POST['vc']); ?>"><br>

Véggáz kilépési sebessége [m/s]: *<input type="number" step="any" min="0"  name="ks" id="ks" value="<?php echo htmlspecialchars($_POST['ks']); ?>"><br>

Szélsebesség [m/s]: <input type="number" step="any" min="0"  name="szs" id="szs" value="<?php echo htmlspecialchars($_POST['szs']); ?>"><br>

Környezeti levegő hőmérséklete [°C]: *<input type="number" step="any" name="kc" id="kc" value="<?php echo htmlspecialchars($_POST['kc']); ?>"><br>

Stabilitási kategória: <SELECT name="sk" size=1 id="sk">
    <OPTION value=1 <?php if($stab==1) echo "selected"; ?>> 1 </option>
    <OPTION value=2 <?php if($stab==2) echo "selected"; ?>> 2 </option>
    <OPTION value=3 <?php if($stab==3) echo "selected"; ?>> 3 </option>
    <OPTION value=4 <?php if($stab==4) echo "selected"; ?>> 4 </option>
    <OPTION value=5 <?php if($stab==5) echo "selected"; ?>> 5 </option>
    <OPTION value=6 <?php if($stab==6) echo "selected"; ?>> 6 </option>
    <OPTION value=7 <?php if($stab==7) echo "selected"; ?>> 7 </option>
</SELECT> <br>

Talajfelszín érdesség: <SELECT name="tf" size=1 id="tf">
    <OPTION value=1 <?php if($felsz==1) echo "selected"; ?>> Sík </option>
    <OPTION value=2 <?php if($felsz==2) echo "selected"; ?>> Erdő </option>
    <OPTION value=3 <?php if($felsz==3) echo "selected"; ?>> Település </option>
    <OPTION value=4 <?php if($felsz==4) echo "selected"; ?>> Város </option>
    <OPTION value=5 <?php if($felsz==5) echo "selected"; ?>> Nagyváros </option>
</SELECT> <br>

Periódus: <SELECT name="pe" size=1 id="pe" onchange="onPeriodChange()">
    <OPTION value=1 <?php if($period==1) echo "selected"; ?>> Órás </option>
    <OPTION value=2 <?php if($period==2) echo "selected"; ?>> Napi </option>
    <OPTION value=3 <?php if($period==3) echo "selected"; ?>> Éves </option>
</SELECT> <br>

Modell max. távolság [m]: *<input type="number" step="any" min="0"  name="mx" id="mx" value="<?php if(isset($_POST['mx'])){echo ceil(htmlspecialchars($_POST['mx']));} else{echo "300";} ?>"><br>

Emisszió tömegárama [mg/s]: *<input type="number" step="any" min="0"  name="em" id="em" value="<?php echo htmlspecialchars($_POST['em']); ?>"><br>

Szennyezőanyag megnevezése: <SELECT name="szm" size=1 id="szm" onchange="onSelectChange()">
    <OPTION value=1 <?php if($szennyanyag==1) echo "selected"; ?>>  </option>
    <OPTION value=2 <?php if($szennyanyag==2) echo "selected"; ?>> Kén-dioxid </option>
    <OPTION value=3 <?php if($szennyanyag==3) echo "selected"; ?>> Nitrogén-dioxid </option>
    <OPTION value=4 <?php if($szennyanyag==4) echo "selected"; ?>> Szén-monoxid </option>
    <OPTION value=5 <?php if($szennyanyag==5) echo "selected"; ?>> Szálló por </option>
    <OPTION value=6 <?php if($szennyanyag==6) echo "selected"; ?>> Egyéb </option> 
</SELECT>
	<input id="textbox" type="text" name="" value="" hidden="true"><br>
	 
Imissziós határérték [µg/m3]:<input type="text" name="imh" id="imh" readonly="true" value="<?php echo $imh; ?>"><br>

Alapállapoti terheltség [µg/m3]: <input type="number" step="any" min="0"  name="alt" id="alt" value="<?php echo htmlspecialchars($_POST['alt']); ?>"><br>

<input type="submit" class="button" name="submit">

<?php include('validation.php'); ?>
<?php
 if(!empty($success_message)) { ?>	
  <?php if(isset($success_message)) echo "<h4 style='color: green;'>".$success_message."</h4>"; ?>
  <?php } ?>
  <?php if(!empty($error_message)) { ?>	
  <?php if(isset($error_message)) echo "<h4 style='color: red;'>".$error_message."</h4>"; ?>
  <?php } ?>


</form>

