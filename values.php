<?php
error_reporting(0);
//Nem számítandó mezőértékek
    $em=(float)$_POST['em'];
    $km=(float)$_POST['km'];
    $ks=(float)$_POST['ks'];
    $szs=(float)$_POST['szs'];
    $d=(float)$_POST['d'];
    $mx=(float)$_POST['mx'];
    $vc=(float)$_POST['vc'];
    $kc=(float)$_POST['kc'];
    $stab= $_POST['sk'];
    $felsz= $_POST['tf'];
    $period=$_POST['pe'];
    $szennyanyag=$_POST['szm'];
    $alt=(float)$_POST['alt'];
    $imh=(float)$_POST['imh'];
    $Ts_K=(float)$_POST['vc']+273.15;
    $Th_K=(float)$_POST['kc']+273.15;
    $st='';
    $z0='';

//Stabilitás +
switch ($stab) {
    case 1:
        $st=0.464;
        break;
    case 2:
        $st=0.446;
        break;
    case 3:
        $st=0.427;
        break;
    case 4:
        $st=0.384;
        break;
    case 5:
        $st=0.343;
        break;
    case 6:
        $st=0.282;
        break;
    case 7:
        $st=0.17;
        break;
}


//Talajfelszín érdesség
switch ($felsz) {
    case 1:
        $z0=0.1;
        break;
    case 2:
        $z0=0.3;
        break;
    case 3:
        $z0=1;
        break;
    case 4:
        $z0=1.5;
        break;
    case 5:
        $z0=3;
        break;
}

    //Fix értékek számítása//

//Qh számítása
$Qh=271*(($Ts_K-$Th_K)/$Ts_K)*$d*$d*$ks;

//Hk számítása - HELGA - feltétel, mi van ha több mint 1,5
$Hk=0;
if($ks > 1.5*$szs){
$Hk=$km;
}else {
$Hk = $km+2*(($ks/$szs)-1.5)*$d;
}

//delta H számítása
$gyokQh=sqrt($Qh);
$powU=pow($szs, 3/4);
$dH=2.7*$gyokQh/$powU;

//Effektív kéménymagasság
$H=$Hk+$dH;

//Csúcs- és C koncentráció keresése
$maxkonc=0;
$maxkonc_tav=0;
$lastkonc=0;
$c_tav=0;
$konc=0;
$vert=0;
$vertA=0;
$hat=0;

for ($x = 1; $x <= 1000; $x++) {
$Y_kitevo=0.367*(2.5-$st);

$szigY=0.08*(((6*(pow($st, -0.3)))+1-(log($H/$z0)))*(pow($x, $Y_kitevo)));

$Z_kitevo=1.55*exp(-2.35*$st);

$szigZ=0.38*pow($st, 1.3)*(8.7-log($H/$z0))*pow($x, $Z_kitevo);

if($period==1){
$konc=(($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ));

}elseif($period==2){
$konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/24),0.45));

}else{
$konc=((($em*1000)/(pi()*$szigY*$szigZ*$szs))*exp(-0.5*($H/$szigZ)*($H/$szigZ)))*(pow((1/8760),0.45));
$konc=round($konc, 8);
}

    if ($konc > $maxkonc){

        $maxkonc=$konc;

        $maxkonc_tav=$x;
    }

    if (0.8*$maxkonc<=$lastkonc && 0.8*$maxkonc>=$konc) {

        $c_tav=$x;
    };

    $lastkonc=$konc;

}

$c=round(0.8*$maxkonc,4);

$ht='';

$hatasterulet= null;
if($imh*0.1 < $maxkonc){
    $hatasterulet = 0.1*$imh;
    $ht='\nHatásterület A: '.($hatasterulet).' m';
} elseif(($imh-$alt)*0.2 < $maxkonc){
    $hatasterulet = ($imh-$alt)*0.2;
    $ht='\nHatásterület B: '.($hatasterulet).' m';
}

if ($konc > $maxkonc){

    $maxkonc=$konc;

    $maxkonc_tav=$x;
}


$maxtick=$maxkonc*1.1;

if ($maxkonc < 10) {

    $maxtick=round($maxtick,1);



} elseif ($maxkonc < 100 && $maxtick>=10) {

    $maxtick=round($maxtick,0);

} else {

    $maxtick=round($maxtick,0);

}
